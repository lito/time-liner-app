# README #

Time-Liner is a time tracking app for iOS (MVP) with using React Native. The main functionality includes:

* user login and session management
* manual time log based on user activities
* projects overview
* reporting via email

### What is this repository for? ###

* iOS app written with using React Native, Redux, Material UI
* Communication with a backend via REST API
* v2.0

### How do I get set up? ###

* npm i
* npm run start