/**
 * TimeLiner React Native app
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react'
import { StyleSheet, AppRegistry } from 'react-native';
import { setTheme, MKColor } from 'react-native-material-kit';
import { Router, Scene } from 'react-native-router-flux';
import { Provider } from 'react-redux';
import configureStore from './lib/configureStore';
import AuthInitialState from './reducers/auth/initialState';
import ProjectInitialState from './reducers/project/initialState';
import App from './scenes/App';
import Login from './scenes/Login';
import Logout from './scenes/Logout';
import Start from './scenes/Start';
import Projects from './scenes/Projects';
import Tasks from './scenes/Tasks';
import TabIcon from './components/TabIcon';

setTheme({
  primaryColor: MKColor.Purple,
  primaryColorRGB: MKColor.RGBPurple,
  accentColor: MKColor.Amber,
});

const styles = StyleSheet.create({
  tabBar: {
    height: 70,
  },
});

const getInitialState = () => {
  const initState = {
    auth: new AuthInitialState(),
    project: new ProjectInitialState(),
  }
  return initState;
};
export default function native (platform) {
  class TimeLiner extends React.Component {
    render() {
      const store = configureStore(getInitialState());
      return (
        <Provider store={store}>
            <Router sceneStyle={{ backgroundColor: 'white' }}>
              <Scene key='root'>
                <Scene key='App'
                  title={'Timeline loading ...'}
                  component={App}
                  type='replace'
                  tabs
                  hideNavBar
                  initial
                  tabBarStyle={styles.tabBar}
                   />
                <Scene key='Login'
                  title={'Login'}
                  component={Login}
                  hideNavBar
                  type='replace' />
                <Scene key='Projects'
                  title={'Projects'}
                  component={Projects} />
                <Scene key='Tasks'
                  title={'Tasks'}
                  component={Tasks} />
                <Scene key='Tabbar'
                  type='replace'
                  tabs
                  tabBarStyle={styles.tabBar}
                  default='Start'>
                  <Scene key='Start'
                    title={'Time Log'}
                    iconName={'av-timer'}
                    icon={TabIcon}
                    hideNavBar
                    initial={true}
                    component={Start} />
                  <Scene key='Reports'
                    title={'Reports'}
                    iconName={'library-books'}
                    icon={TabIcon}
                    hideNavBar
                    component={Login} />
                  <Scene key='Settings'
                    title={'Settings'}
                    iconName={'toc'}
                    icon={TabIcon}
                    hideNavBar
                    component={Login} />
                  <Scene key='Logout'
                    title={'Logout'}
                    icon={TabIcon}
                    iconName={'exit-to-app'}
                    hideNavBar
                    component={Logout} />
                </Scene>
              </Scene>
            </Router>
        </Provider>
      );
    }
  }

  AppRegistry.registerComponent('TimeLiner', () => TimeLiner);
}
