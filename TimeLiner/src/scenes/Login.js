import React from 'react';
import {
  Text,
  View
} from 'react-native';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import LoginForm from '../components/LoginForm';
import * as authActions from '../reducers/auth/actions';
import createStyles from '../lib/createStyles';

const styles = createStyles();

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...authActions }, dispatch)
  }
};

const mapStateToProps = (state) => {
  return {
    isFetching: state.auth.isFetching
  }
};

class LoginScene extends React.Component{
  render() {
    const onFormSubmit = (username, password) => {
      this.props.actions.login(username, password);
    }
    return (
      <View style={styles.container}>
        <Text style={styles.summary}>{this.props.title}</Text>
        <LoginForm
          onSubmit={onFormSubmit}
          auth={this.props.auth}
        />
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScene);
