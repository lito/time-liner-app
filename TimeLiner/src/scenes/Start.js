import React from 'react';
import { Text, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as projectActions from '../reducers/project/actions';
import createStyles from '../lib/createStyles';
import StartLog from '../components/StartLog';
const styles = createStyles();

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...projectActions }, dispatch)
  }
};

const mapStateToProps = (state) => {
  return {
    isFetching: state.project.isFetching,
    activeProjects: state.project.projects
      && state.project.projects.filter(project => project.inProgress),
    currentProject: state.project.currentProject,
    currentTask: state.project.currentTask,
  }
};

class StartScene extends React.Component{
  componentDidMount() {
    this.props.actions.getProjects();
  }
  onStartLog() {
    const self = this;
    const currentProject = Object.assign({}, this.props.currentProject);
    const currentTask = currentProject.tasks.find(task => task.id == self.props.currentTask.id );
    currentTask.inProgress = true;
    this.props.actions.startLog(currentProject);
  }
  onStopLog() {
    this.props.actions.stopLog();
  }
  onPressProject() {
    this.props.actions.navigateToProjects(this.props.activeProjects);
  }
  onPressTask() {
    this.props.actions.navigateToTasks(this.props.currentProject.tasks);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.summary}>{this.props.title}</Text>
        <StartLog
          projects={this.props.activeProjects}
          currentProject={this.props.currentProject}
          currentTask={this.props.currentTask}
          onPressProject={this.onPressProject.bind(this)}
          onPressTask={this.onPressTask.bind(this)}
          onStartLog={this.onStartLog.bind(this)}
          onStopLog={this.onStopLog.bind(this)}
        />
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartScene);
