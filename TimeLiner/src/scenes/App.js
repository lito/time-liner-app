import React from 'react';
import {
  Text,
  View
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActions from '../reducers/auth/actions';
import createStyles from '../lib/createStyles';

const styles = createStyles();

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...authActions }, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    auth: {
      isFetching: state.auth.isFetching
    }
  }
}

class App extends React.Component {
  componentDidMount () {
    this.props.actions.checkAccessToken();
  }
	render() {
		return (
      <View style={styles.container}>
        <Text style={styles.summary}>Timeliner App</Text>
      </View>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
