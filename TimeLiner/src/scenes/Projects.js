import React from 'react';
import { View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as projectActions from '../reducers/project/actions';
import ItemList from '../components/ItemList';
import createStyles from '../lib/createStyles';

const styles = createStyles();

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...projectActions }, dispatch)
  }
};

const mapStateToProps = (state) => {
  return {
    isFetching: state.project.isFetching
  }
};

class Projects extends React.Component{
  onPressProject(project) {
    this.props.actions.selectCurrentProject(project);
  }
  render() {
    return (
      <View style={{
          ...styles.container,
          marginTop: 44
        }}>
        {this.props.projects &&
          <ItemList
            list={this.props.projects}
            onPress={this.onPressProject.bind(this)}
          />
        }
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Projects);
