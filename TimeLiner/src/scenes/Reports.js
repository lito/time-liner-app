
'use strict';

var React = require('react-native');

var {
  AppRegistry,
  StyleSheet,
  ScrollView,
  Text,
  NavigatorIOS,
  View,
} = React;


var styles = StyleSheet.create({
  wrapper: {
    flex: .1,
    justifyContent: 'center',
    alignItems: 'center'
    
  },
  header: {
    fontSize: 10,
    textAlign: 'center',
    color: '#111',
    padding: 100
  }
});

class Reports extends React.Component{
	render() {
    
		return (
			<View style={styles.wrapper}>
  				<Text>
          Reports
          </Text>
			</View>
		);
	}
};	

module.exports = Reports;