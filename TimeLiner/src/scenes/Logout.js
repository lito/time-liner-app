import React from 'react';
import { Text, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActions from '../reducers/auth/actions';
import createStyles from '../lib/createStyles';

const styles = createStyles();

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...authActions }, dispatch)
  }
};

const mapStateToProps = (state) => {
  return {
    isFetching: state.auth.isFetching
  }
};

class LogoutScene extends React.Component{
  componentDidMount() {
    this.props.actions.logout();
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.summary}>{this.props.title}</Text>
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoutScene);
