
'use strict';

var React = require('react-native');

var User = require('../Utils/User');

var ProjectList = require('../Components/ProjectList');

var {
  AppRegistry,
  StyleSheet,
  ScrollView,
  Text,
  NavigatorIOS,
  View,
} = React;

var styles = StyleSheet.create({
  wrapper: {
    flex: 1, 
    
  },
  header: {
    fontSize: 10,
    textAlign: 'center',
    color: '#111',
    padding: 100
  },
  button: {
    margin: 7,
    padding: 5,
    alignItems: 'center',
    backgroundColor: '#eaeaea',
    borderRadius: 1,
  }
});

class Settings extends React.Component{
	render() {
		return (
			<View style={styles.wrapper}>
  				<Text>
          Settings
          </Text>
          <ProjectList navigator={this.props.navigator}/>
			</View>
		);
	}
};	

module.exports = Settings;