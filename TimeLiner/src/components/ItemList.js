import React from 'react';
import { View, ListView } from 'react-native';
import ListItem from './ListItem';
import createStyles from '../lib/createStyles';

const styles = createStyles();
const dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class ItemList extends React.Component{
  render() {
    return (
      <View>
        <ListView
          style={{
            flexDirection: 'column',
          }}
          removeClippedSubviews={true}
          scrollEnabled={true}
          dataSource={dataSource.cloneWithRows(this.props.list)}
          renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
          renderRow={(rowData) => {
          return <ListItem
              style={styles.wrapper}
              onPress={this.props.onPress}
              {...rowData} />
            }} />
      </View>
    );
  }
}

export default ItemList;
