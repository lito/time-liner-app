import keyMirror from 'key-mirror';

export default keyMirror({

  ACCESS_REQUEST: null,
  ACCESS_SUCCESS: null,
  ACCESS_FAILURE: null,

  DELETE_TOKEN_REQUEST: null,
  DELETE_TOKEN_SUCCESS: null,

  ON_LOGIN_STATE_CHANGE: null,
  LOGOUT: null,

  ON_AUTH_FORM_FIELD_CHANGE: null,

  LOGIN_REQUEST: null,
  LOGIN_SUCCESS: null,
  LOGIN_FAILURE: null,

  LOGOUT_REQUEST: null,
  LOGOUT_SUCCESS: null,
  LOGOUT_FAILURE: null,

  LOGGED_IN: null,
  LOGGED_OUT: null,

  SET_SESSION_TOKEN: null,

  LOAD_PROJECTS_REQUEST: null,
  LOAD_PROJECTS_SUCCESS: null,
  LOAD_PROJECTS_FAILURE: null,

  SET_STATE: null,
  GET_STATE: null,
  SET_STORE: null,
  LOGIN: null,
  PROJECTS: null,
  SET_CURRENT_PROJECT: null,
  SET_CURRENT_TASK: null,
  START_LOG_REQUEST: null,
  START_LOG_SUCCESS: null,
  START_LOG_FAILURE: null,
});
