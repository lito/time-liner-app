import store from 'react-native-simple-store';
import backend from './backend';

export class User {
  constructor() {
    this.SESSION_TOKEN = this.getToken().then(token => { this.SESSION_TOKEN = token; });
  }
  async getToken() {
    return await store.get('SESSION')
      .then((session) => {
        return session && session.token;
      })
      .catch(error => {
        debugger
      });
  }
  async login(username, password) {
    return await backend.sendRequest(
      'users/auths?login=' + username + "&password=" + password,
      'POST'
    ).then(function (payload) {
      if (payload.token) {
        store.save('SESSION', {
          token: payload.token
        });
        this.SESSION_TOKEN = payload.token;
      }
      return payload;
    });
  }
  async logout() {
    return await store.delete('SESSION');
  }
  async getProjects() {
    return await backend.sendRequest(
      'contents?type=project&token=' + this.SESSION_TOKEN,
    ).then(function (payload) {
      return payload;
    });
  }
  async updateProject (project) {
    return await backend.sendRequest(
      `contents/${project.id}?type=project&token=${this.SESSION_TOKEN}`,
      'PUT',
      JSON.stringify(project)
    ).then(function (payload) {
      console.log('DEBUG: ', project, payload);
      return payload;
    });
  }
}

export default new User();
