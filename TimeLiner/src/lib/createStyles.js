import { StyleSheet } from 'react-native';
import { MKColor } from 'react-native-material-kit';

const defaultStyles = {
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  header: {
    fontSize: 10,
    color: '#333',
    padding: 100
  },
  summary: {
    fontSize: 28,
    fontWeight: 'bold',
    textAlign: 'center',
    color: MKColor.BlueGrey
  },
  textfield: {
    height: 28,
    marginTop: 32,
  },
  textfieldWithFloatingLabel: {
    height: 48,
    marginTop: 10,
  },
  buttonText: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 13,
    textAlign: 'center'
  },
  separator: {
    borderBottomWidth: 1,
    borderBottomColor: MKColor.Silver,
  }
};

export default function (customStyle) {
  return Object.assign({}, defaultStyles, StyleSheet.create(customStyle));
}
