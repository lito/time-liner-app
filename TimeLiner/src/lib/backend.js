export class Backend {
  constructor() {
    this.REQUEST_URL = 'http://time-liner.esy.es/api/';
  }

  async sendRequest(url, method = 'GET', body) {
    const response = await fetch(
      this.REQUEST_URL + url,
      {
        method,
        body
      });
    return response.json();
  }
}

export default new Backend();
