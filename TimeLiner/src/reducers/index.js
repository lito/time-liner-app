import auth from './auth/reducer';
import project from './project/reducer';

import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  auth,
  project,
});

export default rootReducer;
