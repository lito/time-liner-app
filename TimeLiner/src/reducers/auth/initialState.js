const { Record } = require('immutable');

const {
  LOGIN
} = require('../../lib/constants').default;

export default Record({
  state: LOGIN,
  disabled: false,
  error: null,
  isValid: false,
  isFetching: false,
  isLoggedIn: false,
});
