const InitialState = require('./initialState').default;

const {
  SESSION_TOKEN_REQUEST,
  SESSION_TOKEN_SUCCESS,
  SESSION_TOKEN_FAILURE,
  LOGOUT,
  LOGIN,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  SET_STATE,
} = require('../../lib/constants').default;

const initialState = new InitialState();

export default function authReducer (state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState.mergeDeep(state)

  switch (action.type) {
    case SESSION_TOKEN_REQUEST:
    case LOGOUT_REQUEST:
    case LOGIN_REQUEST: {
      let nextState = state
        .setIn(['isFetching'], true)
        .setIn(['error'], null)
      return nextState;
    }

    case LOGIN:
      return state.setIn(['state'], action.type)
      .setIn(['error'], null);

    case LOGOUT:
      return state.setIn(['state'], action.type)
        .setIn(['error'], null);

    case SESSION_TOKEN_SUCCESS:
    case SESSION_TOKEN_FAILURE:
    case LOGIN_SUCCESS: {
      //state.setIn(['isLoggedIn'], true);
    }
    case LOGOUT_SUCCESS:
      return state.setIn(['isFetching'], false);

    case LOGOUT_FAILURE:
    case LOGIN_FAILURE:
      return state.setIn(['isFetching'], false)
      .setIn(['error'], action.payload);

    case SET_STATE: {
      return next;
    }
  }
  return state;
}
