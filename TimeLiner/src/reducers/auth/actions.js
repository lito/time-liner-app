const {
  LOGIN,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  ACCESS_REQUEST,
  ACCESS_SUCCESS,
  ACCESS_FAILURE,
  LOGOUT
} = require('../../lib/constants').default;

import user from '../../lib/user';
import { Actions } from 'react-native-router-flux';

export function loginState () {
  return {
    type: LOGIN
  }
}

export function loginRequest () {
  return {
    type: LOGIN_REQUEST
  }
}

export function loginSuccess (payload) {
  return {
    type: LOGIN_SUCCESS,
    payload
  }
}

export function loginFailure (error) {
  return {
    type: LOGIN_FAILURE,
    payload: error
  }
}

export function accessTokenSuccess (payload) {
  return {
    type: ACCESS_SUCCESS,
    payload
  }
}

export function accessTokenFailure (error) {
  return {
    type: ACCESS_FAILURE,
    payload: error
  }
}

export function logoutState () {
  return {
    type: LOGOUT
  }
}

export function checkAccessToken () {
  return dispatch => {
      return user.getToken()
        .then ((token) => {
          if (token) {
            dispatch(accessTokenSuccess(token));
            dispatch(logoutState());
            Actions.Tabbar();
          }
          else {
            dispatch(accessTokenFailure('error'));
            Actions.Login();
          }
      });
    }
}

export function login (username, password) {
  return dispatch => {
    dispatch(loginRequest());
    return user.login(username, password)
      .then(function (response) {
        dispatch(loginSuccess(response))
        dispatch(logoutState());
        Actions.Tabbar()
      })
      .catch((error) => {
        debugger
        dispatch(loginFailure(error));
      });
  }
}

export function logout () {
  return dispatch => {
    return user.logout()
      .then(function () {
        dispatch(loginState());
        Actions.Login();
      })
      .catch((error) => {
        debugger
        dispatch(loginFailure(error));
        Actions.Tabbar();
      });
  }
}
