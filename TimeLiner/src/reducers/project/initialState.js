const { Record } = require('immutable');

export default Record({
  isFetching: false,
  error: null,
  projects: null,
  currentProject: null,
  currentTask: null,
});
