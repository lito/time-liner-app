const {
  LOAD_PROJECTS_REQUEST,
  LOAD_PROJECTS_SUCCESS,
  LOAD_PROJECTS_FAILURE,
  PROJECTS,
  SET_CURRENT_PROJECT,
  SET_CURRENT_TASK,
  START_LOG_REQUEST,
  START_LOG_SUCCESS,
  START_LOG_FAILURE
} = require('../../lib/constants').default;

import user from '../../lib/user';
import { Actions } from 'react-native-router-flux';

export function projectsState() {
  return {
    type: PROJECTS
  };
}

export function loadProjectsRequest() {
  return {
    type: LOAD_PROJECTS_REQUEST
  };
}

export function loadProjectsSuccess(payload) {
  return {
    type: LOAD_PROJECTS_SUCCESS,
    payload
  };
}

export function loadProjectsFailure(error) {
  return {
    type: LOAD_PROJECTS_FAILURE,
    payload: error
  };
}

export function setCurrentProject(project) {
  return {
    type: SET_CURRENT_PROJECT,
    payload: project
  };
}

export function setCurrentTask(task) {
  return {
    type: SET_CURRENT_TASK,
    payload: task
  };
}

export function startLogRequest() {
  return {
    type: START_LOG_REQUEST
  };
}

export function startLogSuccess(payload) {
  return {
    type: START_LOG_SUCCESS,
    payload
  };
}

export function startLogFailure(error) {
  return {
    type: START_LOG_FAILURE,
    payload: error
  };
}

export function getProjects() {
  return dispatch => {
    dispatch(loadProjectsRequest());
    return user
      .getProjects()
      .then(function(response) {
        dispatch(loadProjectsSuccess(response));
      })
      .catch(error => {
        dispatch(loadProjectsFailure(error));
      });
  };
}

export function navigateToProjects(projects) {
  return dispatch => {
    dispatch(projectsState());
    Actions.Projects({
      projects,
      title: 'Select project'
    });
  };
}

export function navigateToTasks(tasks) {
  return dispatch => {
    //dispatch(projectsState());
    Actions.Tasks({
      tasks,
      title: 'Select task'
    });
  };
}

export function selectCurrentProject(project) {
  return dispatch => {
    dispatch(setCurrentProject(project));
    //dispatch(login());
    Actions.pop();
  };
}

export function selectCurrentTask(task) {
  return dispatch => {
    dispatch(setCurrentTask(task));
    //dispatch(login());
    Actions.pop();
  };
}

export function startLog(project) {
  return dispatch => {
    dispatch(startLogRequest(project));
    return user
      .updateProject(project)
      .then(function(response) {
        dispatch(startLogSuccess(response.element));
      })
      .catch(error => {
        dispatch(startLogFailure(error));
      });
  };
}
