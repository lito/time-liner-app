const InitialState = require('./initialState').default;

const {
  LOAD_PROJECTS_REQUEST,
  LOAD_PROJECTS_SUCCESS,
  LOAD_PROJECTS_FAILURE,
  SET_CURRENT_PROJECT,
  SET_CURRENT_TASK,
  START_LOG_REQUEST,
  START_LOG_SUCCESS,
  START_LOG_FAILURE,
} = require('../../lib/constants').default;

const initialState = new InitialState();

export default function projectReducer (state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState.mergeDeep(state)

  switch (action.type) {
    case LOAD_PROJECTS_REQUEST: {
      let nextState = state
        .setIn(['isFetching'], true)
      return nextState;
    }
    case LOAD_PROJECTS_SUCCESS: {
      console.log(action.payload);
      let nextState = state
        .setIn(['projects'], action.payload)
        .setIn(['isFetching'], true)
      return nextState;
    }
    case LOAD_PROJECTS_FAILURE:
      return state.setIn(['error'], action.payload)
        .setIn(['isFetching'], false);
    case SET_CURRENT_PROJECT: {
      return state.setIn(['currentProject'], action.payload);
    }
    case SET_CURRENT_TASK: {
      return state.setIn(['currentTask'], action.payload);
    }
    case START_LOG_REQUEST: {
      return state.setIn(['isFetching'], true);
    }
    case START_LOG_SUCCESS: {
      return state.setIn(['currentProject'], action.payload)
        .setIn(['isFetching'], false);
    }
    case START_LOG_FAILURE: {
      return state.setIn(['error'], action.payload)
        .setIn(['isFetching'], false);
    }

  }
  return state;
}
