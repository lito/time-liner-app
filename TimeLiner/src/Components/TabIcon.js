
import React from 'react'
import {
  StyleSheet,
  View,
  Text } from 'react-native';
import { MKColor } from 'react-native-material-kit';
import Icon from 'react-native-vector-icons/MaterialIcons';


const styles = StyleSheet.create({
  TabIcon: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'center',
  },
});

class TabIcon extends React.Component {
  static defaultProps = { size: 30 };
  render () {
    var color = this.props.selected ? MKColor.BlueGrey : MKColor.Purple;
    return (
      <View style={styles.TabIcon}>
        <Icon style={{color}} name={this.props.iconName} size={this.props.size} />
        <Text style={{color}}>{this.props.title}</Text>
      </View>
     )
  }
}

export default TabIcon;
