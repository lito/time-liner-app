import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import createStyles from '../lib/createStyles';

const styles = createStyles({
  flexBox: {
    flex: 1,
    flexDirection: 'row',
  },
  flexItem: {
    flex: 1,
    textAlign: 'center',
    paddingTop: 18,
    paddingBottom: 18,
  },
});

class ListItem extends React.Component{
  onPress(){
    this.props.onPress({
      ...this.props
    });
  }
  renderColoredBorder() {
    return (
      <View style={{
          width: 3,
          backgroundColor: this.props.color,
        }}
      />
    );
  }
  render() {
    return (
      <TouchableHighlight
  underlayColor="white"
        onPress={this.onPress.bind(this)}
      >
        <View style={styles.flexBox}>
          {this.props.color && this.renderColoredBorder()}
          <Text style={styles.flexItem}>{this.props.name}</Text>
          {this.props.color && this.renderColoredBorder()}
        </View>
      </TouchableHighlight>
    );
  }
}

export default ListItem;
