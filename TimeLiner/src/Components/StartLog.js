import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { MKColor, MKButton } from 'react-native-material-kit';
import createStyles from '../lib/createStyles';
import ListItem from './ListItem';
import TabIcon from './TabIcon';

const styles = createStyles({
  container: {
    paddingTop: 20
  },
  buttonContainer: {
    marginBottom: 10,
  },
  buttonText: {
    color: MKColor.Purple,
    textAlign: 'center',
    padding: 8
  }
});

const SelectButton = MKButton.flatButton()
  .build();

class StartLog extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  isValid() {

  }
  onStopLog() {
    this.props.onStopLog();
  }
  onStartLog() {
    this.props.onStartLog();
  }
  onPressProject() {
    this.props.onPressProject();
  }
  onPressTask() {
    this.props.onPressTask();
  }
  renderCurrentProject(){
    const currentProject = this.props.currentProject;
    return currentProject ?
      <ListItem
        name={currentProject.name}
        color={currentProject.color}
        onPress={this.onPressProject.bind(this)}
      /> :
      <SelectButton
        borderWidth={1}
        borderColor='#ebebeb'
        backgroundColor='#ebebeb'
        textAlign='center'
        style={styles.buttonContainer}
        onPress={this.onPressProject.bind(this)}>
        <Text style={styles.buttonText}>Select a project</Text>
      </SelectButton>;
  }
  renderCurrentTask(){
    const currentTask = this.props.currentTask;
    return currentTask ?
      <ListItem
        name={currentTask.name}
        color={currentTask.color}
      /> :
      <SelectButton
        borderWidth={1}
        borderColor='#ebebeb'
        textAlign='center'
        style={styles.buttonContainer}
        onPress={this.onPressTask.bind(this)}
      >
        <Text style={styles.buttonText}>Select a task</Text>
      </SelectButton>;
  }
  renderLogButton() {
    return (
      <TouchableHighlight
        underlayColor="white"
        style={styles.container}
        onPress={this.onStartLog.bind(this)}
      >
        <View style={styles.flexBox}>
          <TabIcon
            iconName="watch-later"
            title="Start"
            size={130}
          />
        </View>
      </TouchableHighlight>
    );
  }
  render () {
    return (
      <View style={styles.container}>
        {this.renderCurrentProject()}
        {this.props.currentProject && this.renderCurrentTask()}
        {this.props.currentProject && this.props.currentTask && this.renderLogButton()}
      </View>
    );
	}
}

export default StartLog;
