import React from 'react';
import { Text, View } from 'react-native';
import { MKButton, MKColor, MKTextField } from 'react-native-material-kit';
import createStyles from '../lib/createStyles';

const styles = createStyles({
  buttonContainer: {
    marginTop: 30,
    paddingTop: 15,
    paddingBottom: 15
  }
});

const SubmitButton = MKButton.coloredButton().build();

const UsernameInput = MKTextField.textfieldWithFloatingLabel().build();
const PasswordInput = MKTextField.textfieldWithFloatingLabel().build();

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isValid: false
    };
  }
  isValid() {
    this.setState({
      isValid:
        this.refs.username.bufferedValue && this.refs.password.bufferedValue
    });
  }
  onSubmitPress() {
    this.props.onSubmit(
      this.refs.username.bufferedValue,
      this.refs.password.bufferedValue
    );
  }
  render() {
    return (
      <View>
        <UsernameInput
          ref="username"
          autoCapitalize="none"
          tintColor={MKColor.Lime}
          textInputStyle={{ color: MKColor.Orange }}
          placeholder="Username…"
          onTextChange={this.isValid.bind(this)}
          editable={!this.props.isFetching}
          autoCorrect={false}
          style={styles.textfieldWithFloatingLabel}
        />
        <PasswordInput
          ref="password"
          password
          tintColor={MKColor.Lime}
          textInputStyle={{ color: MKColor.Orange }}
          placeholder="Password…"
          onTextChange={this.isValid.bind(this)}
          editable={!this.props.isFetching}
          style={styles.textfieldWithFloatingLabel}
        />
        <SubmitButton
          coloredButton
          shadowRadius={2}
          shadowOffset={{ width: 0, height: 2 }}
          shadowOpacity={0.7}
          shadowColor="black"
          onPress={this.onSubmitPress.bind(this)}
          style={styles.buttonContainer}
          enabled={!!(this.state.isValid && !this.props.isFetching)}
          backgroundColor={
            this.state.isValid ? MKColor.Purple : MKColor.BlueGrey
          }
        >
          <Text style={styles.buttonText}>SUBMIT</Text>
        </SubmitButton>
      </View>
    );
  }
}

export default LoginForm;
